﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_Visual
{
    public partial class calculadora_cientifica : Form
    {
        double cantidad1, cantidad2, resultado, memoria,factorial = 1, contador = 0;
        string operador;
        public calculadora_cientifica()
        {
            InitializeComponent();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + "1";

        }

        private void button11_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + "2";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + "3";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + "4";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + "5";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + "6";
        }

        private void button20_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + "7";
        }

        private void button19_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + "8";
        }

        private void button14_Click(object sender, EventArgs e)
        {
            operador = "-";
            cantidad1 = double.Parse(pantalla1.Text);
            pantalla1.Clear();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            operador = "*";
            cantidad1 = double.Parse(pantalla1.Text);
            pantalla1.Clear();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            operador = "/";
            cantidad1 = double.Parse(pantalla1.Text);
            pantalla1.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cantidad2 = double.Parse(pantalla1.Text);
            switch(operador)
            {
                case "+":
                    resultado = cantidad1 + cantidad2;
                    pantalla1.Text = resultado.ToString();
                    break;
                case "-":
                    resultado = cantidad1 - cantidad2;
                    pantalla1.Text = resultado.ToString();
                    break;
                case "*":
                    resultado = cantidad1 * cantidad2;
                    pantalla1.Text = resultado.ToString();
                    break;
                case "/":
                    resultado = cantidad1 / cantidad2;
                    pantalla1.Text = resultado.ToString();
                    break;
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Sqrt(cantidad1).ToString();
        }

        private void button25_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Pow(cantidad1, 3).ToString();
        }

        private void button24_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Pow(cantidad1, 2).ToString();
        }

        private void button28_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Pow(10, cantidad1).ToString();
        }

        private void button27_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Sin(cantidad1).ToString();
        }

        private void button26_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Cos(cantidad1).ToString();
        }

        private void button30_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Tan(cantidad1).ToString();
        }

        private void button33_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Asin(cantidad1).ToString();
        }

        private void button34_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Acos(cantidad1).ToString();
        }

        private void button35_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Atan(cantidad1).ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pantalla1.Clear();
        }

        private void button22_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Sinh(cantidad1).ToString();
        }

        private void button23_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Cosh(cantidad1).ToString();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + "0";
        }

        private void button18_Click(object sender, EventArgs e)
        {
            cantidad2 = double.Parse(pantalla1.Text);
            resultado = cantidad1 + cantidad2;
            pantalla1.Text = Convert.ToString((cantidad1*cantidad2)/100);

        }

        private void button17_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Exp(cantidad1).ToString();
        }

        private void button31_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Log(cantidad1).ToString();
        }

        private void button32_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Log10(cantidad1).ToString();
        }

        private void button25_Click_1(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            pantalla1.Text = Math.Tanh(cantidad1).ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            pantalla1.Clear();
        }

        private void button29_Click(object sender, EventArgs e)
        {
            cantidad1 = double.Parse(pantalla1.Text);
            resultado = cantidad1;
            for (int i = 1; i <= cantidad1; i++)
            {
                contador = contador + 1;
                factorial = factorial * contador;
            }
            pantalla1.Text = (factorial).ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + "9";
        }

        private void button16_Click(object sender, EventArgs e)
        {
            pantalla1.Text = pantalla1.Text + ".";
            button16.Enabled = false;

        }

        private void button13_Click(object sender, EventArgs e)
        {
            operador = "+";
            cantidad1 = double.Parse(pantalla1.Text);
            pantalla1.Clear();
            
        }
    }
}
