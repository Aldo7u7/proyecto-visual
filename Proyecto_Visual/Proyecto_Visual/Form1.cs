﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_Visual
{
    
    public partial class Form1 : Form
    {
        
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form cientifica = new calculadora_cientifica();
            cientifica.Show();
            MessageBox.Show(" Selecciono La Calculadora Cientifica" );
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form ohm = new Calculadora_ley_ohm();
            ohm.Show();
            MessageBox.Show(" Selecciono La Calculadora De La Ley De Ohm ");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form circuito = new parelelo();
            circuito.Show();
            MessageBox.Show(" Selecciono La Calculadora de Circuitos en Paralelo ");
        }
    }
}
