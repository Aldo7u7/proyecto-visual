﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_Visual
{
    public partial class Calculadora_ley_ohm : Form
    {
        
        public Calculadora_ley_ohm()
        {
            InitializeComponent();
        }

        private void Calculadora_ley_ohm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = false;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                // calculo voltaje
                label2.Text = (Convert.ToDouble(textBox2.Text) * Convert.ToDouble(textBox3.Text)).ToString();
            }
            if (radioButton2.Checked == true)
            {
                // calculo corriente
                label2.Text = (double.Parse(textBox1.Text) / double.Parse(textBox3.Text)).ToString();

            }
            if (radioButton3.Checked == true)
            {
                // calculo resistencia
                label2.Text = (double.Parse(textBox1.Text) / double.Parse(textBox2.Text)).ToString();
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox1.Visible = false;
            textBox2.Visible = true;
            textBox3.Visible = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox1.Visible = true;
            textBox2.Visible = false;
            textBox3.Visible = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
